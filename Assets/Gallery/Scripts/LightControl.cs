﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightControl : MonoBehaviour {
    public GameObject[] lights;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            lights[0].SetActive(!lights[0].activeSelf);
            lights[3].SetActive(!lights[3].activeSelf);
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            lights[0].SetActive(!lights[0].activeSelf);
            lights[1].SetActive(!lights[1].activeSelf);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            lights[1].SetActive(!lights[1].activeSelf);
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            lights[0].SetActive(!lights[0].activeSelf);
            lights[2].SetActive(!lights[2].activeSelf);
        }
    }


    public void OnSwitchPressed (int Index)
    {
        if (Index == 1)
        {
            lights[0].SetActive(!lights[0].activeSelf);
            lights[3].SetActive(!lights[3].activeSelf);
        }
        else if (Index == 2)
        {
            lights[0].SetActive(!lights[0].activeSelf);
            lights[1].SetActive(!lights[1].activeSelf);
        }
        else if (Index == 3)
        {
            lights[1].SetActive(!lights[1].activeSelf);
        }
        else if (Index == 3)
        {
            lights[0].SetActive(!lights[0].activeSelf);
            lights[2].SetActive(!lights[2].activeSelf);
        }

        CheckAllLightsOn();
    }

    bool CheckAllLightsOn()
    {
        if (lights[0].activeSelf && lights[1].activeSelf && lights[2].activeSelf && lights[3].activeSelf)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

}
