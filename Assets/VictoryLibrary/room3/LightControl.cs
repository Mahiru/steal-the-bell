﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightControl : MonoBehaviour {
    public GameObject[] lights;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(KeyCode.Q))
        {
            lights[0].SetActive(!lights[0].activeSelf);
            lights[3].SetActive(!lights[3].activeSelf);
        }
        else if (Input.GetKeyDown(KeyCode.W))
        {
            lights[0].SetActive(!lights[0].activeSelf);
            lights[1].SetActive(!lights[1].activeSelf);
        }
        else if (Input.GetKeyDown(KeyCode.E))
        {
            lights[1].SetActive(!lights[1].activeSelf);
        }
        else if (Input.GetKeyDown(KeyCode.R))
        {
            lights[0].SetActive(!lights[0].activeSelf);
            lights[2].SetActive(!lights[2].activeSelf);
        }
    }

    public void OnButtonPressed(int keyCode)
    {
        switch (keyCode)
        {
            case 1:
                {
                    lights[0].SetActive(!lights[0].activeSelf);
                    lights[3].SetActive(!lights[3].activeSelf);
                }
                break;

            case 2:
                {
                    lights[0].SetActive(!lights[0].activeSelf);
                    lights[1].SetActive(!lights[1].activeSelf);
                }
                break;
            case 3:
                {
                    lights[1].SetActive(!lights[1].activeSelf);
                }
                break;
            case 4:
                {
                    lights[0].SetActive(!lights[0].activeSelf);
                    lights[2].SetActive(!lights[2].activeSelf);
                }
                break;
            default:
                break;
        }
    }

    bool checkAllCleared()
    {
        bool bAllCleared = false;
        if (lights[0].activeSelf && lights[1].activeSelf && lights[2].activeSelf && lights[3].activeSelf)
        {
            bAllCleared = true;
        }
 
        return bAllCleared;
    }

}
